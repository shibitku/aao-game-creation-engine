{
"evidence_balle" : "Weapons|Bullet",
"evidence_balle2" : "Weapons|Bullet 2",
"evidence_bouteilleCassee" : "Weapons|Broken bottle",
"evidence_couteau" : "Weapons|Knife",
"evidence_couteau2" : "Weapons|Knife 2",
"evidence_couteau3" : "Weapons|Knife 3",
"evidence_couteau4" : "Weapons|Knife 4",
"evidence_couteau5" : "Weapons|Knife 5",
"evidence_pistolet" : "Weapons|Pistol",
"evidence_pistolet2" : "Weapons|Pistol 2",
"evidence_pistolet3" : "Weapons|Pistol 3",
"evidence_lameCassee" : "Weapons|Switchblade tip",
"evidence_lanceSamurai" : "Weapons|Samurai Spear",
"evidence_fouet" : "Weapons|Whip",


"evidence_chat" : "Animals|Cat",
"evidence_chien" : "Animals|Dog",
"evidence_chien2" : "Animals|Dog 2",
"evidence_orangOutan" : "Animals|Monkey",
"evidence_ours" : "Animals|Bear",
"evidence_lion" : "Animals|Lion",
"evidence_tigre" : "Animals|Tiger",


"evidence_carte" : "Documents|Card",
"evidence_dossier" : "Documents|Report",
"evidence_enveloppe" : "Documents|Envelope",
"evidence_idCard" : "Documents|ID Card",
"evidence_journal" : "Documents|Newspaper",
"evidence_lettre" : "Documents|Letter",
"evidence_livre" : "Documents|Book",
"evidence_passeport" : "Documents|Passport",
"evidence_photo" : "Documents|Photo",


"evidence_appareilPhoto" : "Devices|Camera",
"evidence_appareilPhoto2" : "Devices|Camera 2",
"evidence_cameraCachee" : "Devices|Video camera",
"evidence_cassette" : "Devices|Tape",
"evidence_talkieWalkie" : "Devices|Transceiver",
"evidence_ecouteTelephonique" : "Devices|Wiretap",
"evidence_portableLana" : "Devices|Cell phone (Lana)",
"evidence_portableMaya" : "Devices|Cell phone (Maya)",
"evidence_portablePhoenix" : "Devices|Cell phone (Phoenix)",
"evidence_traceur" : "Devices|Tracking device",


"evidence_boutonSang" : "Clothing|Bloody button",
"evidence_Echarpe" : "Clothing|Muffler",
"evidence_GantBaseBall" : "Clothing|Baseball glove",
"evidence_chaussureSang" : "Clothing|Bloody shoe",
"evidence_costumeSang" : "Clothing|Bloody robe",
"evidence_echarpeSang" : "Clothing|Bloody muffler",
"evidence_gant" : "Clothing|Glove",
"evidence_lunettesCassees" : "Clothing|Broken glasses",


"evidence_BijouxHorsDePrix" : "Jewels|Priceless jewel",
"evidence_badge" : "Jewels|Badge",
"evidence_bagueFiancailles" : "Jewels|Engagement ring",
"evidence_braceletApollo" : "Jewels|Apollo's bracelet",
"evidence_magatama" : "Jewels|Magatama (Charged)",
"evidence_magatamaDecharge" : "Jewels|Magatama (Not charged)",
"evidence_pendentif" : "Jewels|Pendant",


"evidence_ColdKillerZ" : "Other|Cold Killer X",
"evidence_bouteilleVide" : "Other|Empty bottle",
"evidence_carteDeKiller" : "Other|De Killer's card",
"evidence_cartesAJouer" : "Other|Playing cards",
"evidence_cle" : "Other|Key",
"evidence_coffre" : "Other|Safe",
"evidence_luminol" : "Other|Luminol",
"evidence_marionnette" : "Other|Trilo",
"evidence_montre" : "Other|Watch",
"evidence_morceauxDeVerre" : "Other|Glass shards",
"evidence_papierSang" : "Other|Bloody paper",
"evidence_parapluie" : "Other|Umbrella",
"evidence_passeMagnetique" : "Other|Card key",
"evidence_statuePenseur" : "Other|The Thinker",
"evidence_ticket" : "Other|Ticket",
"evidence_urne" : "Other|Urn",
"evidence_van" : "Other|Van"
}
